'use strict';

var app = angular.module('anomali', []);
app.factory("FormService", function(){

	var destinations = [
		{
			id:"no_selection",
			display:"No Selection"
		},
		{
			id:"arcsight_esm",
			display:"Arcsight ESM",
			fields:[
				{
					display:"Name",
					id:"name",
					type:"input",
					value:null,
					selection:null,
					default:null,
					required:true
				},
				{
					display:"Version",
					id:"version", 
					type:"select",
					value:["6.5", "6.0"],
					selection:null,
					default:null,
					required:false
				},
				{
					display:"Host",
					id:"host",
					type:"input",
					value:null,
					selection:null,
					default:null,
					required:true
				},
				{
					display:"Port",
					id:"port",
					type:"input",
					value:null,
					selection:null,
					default:null,
					required:true
				},
				{
					display:"User",
					id:"user",
					type:"input",
					value:null,
					selection:null,
					default:null,
					required:true
				},
				{
					display:"Password",
					id:"password",
					type:"password",
					value:null,
					selection:null,
					default:null,
					required:true
				},
				{
					display:"Syslog Host",
					id:"syslog_host",
					type:"input",
					value:null,
					selection:null,
					default:null,
					required:false
				},
				{
					display:"Syslog Port",
					id:"syslog_port",
					type:"input",
					value:null,
					selection:null,
					default:null,
					required:false
				},
				{
					display:"Syslog Facility",
					id:"syslog_facility",
					type:"input",
					value:null,
					selection:null,
					default:null,
					required:false
				},
				{
					display:"Filter",
					id:"filter",
					type:"textarea",
					value:null,
					selection:null,
					default:null,
					required:false
				}
			]
		},
		{
			id:"bro_intel",
			display:"Bro Intel"
		},
		{
			id:"cef",
			display:"CEF"
		},
		{
			id:"csv",
			display:"CSV",
			fields:[
				{
					display:"Name",
					id:"name",
					type:"input",
					value:null,
					selection:null,
					default:null,
					required:true
				},
				{
					display:"Directory",
					id:"name",
					type:"input",
					value:null,
					selection:"/tmp",
					default:null,
					required:false
				},
				{
					display:"Filter",
					id:"filter",
					type:"textarea",
					value:null,
					selection:null,
					default:null,
					required:false
				}
			]
		},
		{
			id:"carbon_black",
			display:"Carbon Black"
		},
		{
			id:"cloudera_impala",
			display:"Cloudera Impala"
		},
		{
			id:"nitro",
			display:"Nitro"
		},
		{
			id:"paloalto_networks",
			display:"Paloalto Networks"
		},
		{
			id:"qradar",
			display:"Qradar"
		},
		{
			id:"qradar_api",
			display:"Qradar API"
		},
		{
			id:"splunk",
			display:"Splunk",
			fields:[
				{
					display:"Name",
					id:"name",
					type:"input",
					value:null,
					selection:null,
					default:null,
					required:true
				},				
				{
					display:"Version",
					id:"version", 
					type:"select",
					value:["6.2", "4.9"],
					selection:null,
					default:null,
					required:false
				},			
				{
					display:"Host",
					id:"host",
					type:"input",
					value:null,
					selection:"localhost",
					default:"localhost",
					required:true,
					conditionalFields:[
						{
							display:"SSH Port",
							id:"ssh_port",
							type:"input",
							value:null,
							selection:22,
							default:null,
							required:true
						},
						{
							display:"SSH User",
							id:"ssh_user",
							type:"input",
							value:null,
							selection:null,
							default:null,
							required:true
						},
						{
							display:"SSH Auth",
							id:"ssh_auth",
							type:"select",
							value:["password"],
							selection:null,
							default:null,
							required:false
						},
						{
							display:"SSH Password",
							id:"ssh_password",
							type:"input",
							value:null,
							selection:null,
							default:null,
							required:true
						}
					]
				},				
				{
					display:"Splunk Home",
					id:"splunk_home",
					type:"input",
					value:null,
					selection:"/opt/splunk",
					default:null,
					required:true
				},				
				{
					display:"Filter",
					id:"filter",
					type:"textarea",
					value:null,
					selection:null,
					default:null,
					required:false
				}
			]
		},
		{
			id:"sys_log",
			display:"Syslog"
		}

	];

	function getDestinations(){
		return destinations;
	}

	function submit(destination){
		/**
		 * The last line proves that the selectedDestination object in the controller
		 * is the same as the associated object maintained here in the service.
		 * Submitting is as simple as sending the current model for the form.
		 * @type {[type]}
		 */
		console.log(destination, destination === destinations.indexOf(destination));
	}

	return {
		getDestinations:getDestinations,
		submit:submit
	}
})
app.controller("FormController", function($scope, FormService){

	$scope.destinations = FormService.getDestinations();
	$scope.selectedDestination = $scope.destinations[0];
	$scope.submitEnabled = false;

	// $scope.destinationChanged = function(){
	// 	console.log('Destination Changed using ng-change function.');
	// }

	// $scope.$watch("selectedDestination.fields", function(newValue, oldValue){
	// 	if(newValue){
	// 		console.log("Fields Changed.", newValue)
	// 	}
		
	// }, true);

	function checkValidity(fields){
		var valid = fields.reduce(function(previousValue, currentValue, currentIndex, array){
			if(previousValue === false){
				return false;
			}

			if(currentValue.conditionalFields instanceof Array && currentValue.selection !== currentValue.default){
				var checkConditionalFields = checkValidity(currentValue.conditionalFields);
				if(checkConditionalFields === false){
					return false;
				}
			}

			if(currentValue.required === false){
				return true;
			}else{
				if(currentValue.selection){
					return true;
				}else{
					return false;
				}
			}
		}, true);

		return valid;
	}

	$scope.checkValidity = function(item, validity){
		$scope.submitEnabled = checkValidity($scope.selectedDestination.fields)
	};

	$scope.submit = function(){
		FormService.submit($scope.selectedDestination);
	}

})
app.directive("generateField", function($compile){
	return {
		scope:{
			fieldObject:"=",
			checkValidity:"="
		},
		replace:true,
		restrict: 'A',		
		link: function(scope, element, attributes){


			/**
			 * We nee to build up the template for the different types of form elements, and drop it on the DOM.
			 * @type {String}
			 */
			var template = "<label for=" + scope.fieldObject.id  + ">" + scope.fieldObject.display + 
			"<span ng-if='fieldObject.required' style='color:red'>*</span>"
			": </label><span>";

			switch(scope.fieldObject.type){
				
				case "input":
					template += 
						"<span ng-form name='{{fieldObject.id}}_form'> " + 
						"<input type='text'" + 
						"ng-model='fieldObject.selection' " +
						"name='{{fieldObject.id}}' " +
						"ng-change='checkValidity(fieldObject, {{fieldObject.id}}_form.{{fieldObject.id}}.$valid)' " +
						"id='{{fieldObject.id}}' " +
						"ng-required='fieldObject.required'" +
						"></input>" +
						/**
						 * This div block is for the conditional fields driven by this one.
						 * @type {String}
						 */
						"<div ng-if='fieldObject.default !== null && fieldObject.selection !== fieldObject.default'>" +
						"<div ng-repeat='field in fieldObject.conditionalFields'>" +
						"	<div generate-field check-validity='checkValidity' field-object=field></div>" +
						"</div>" +
						"</div> " +
						// "<div ng-show='{{fieldObject.id}}_form.{{fieldObject.id}}.$invalid'>invalid</div>" +
						"</span>"
					break;				
				case "password":
					template += 
						"<span ng-form name='{{fieldObject.id}}_form'> " + 
						"<input type='password' " + 
						"name='{{fieldObject.id}}' " +
						"ng-change='checkValidity(fieldObject, {{fieldObject.id}}_form.{{fieldObject.id}}.$valid)' " +
						"ng-required='fieldObject.required'" +
						"ng-model='fieldObject.selection'></input>" +
						/**
						 * This div block is for the conditional fields driven by this one.
						 * @type {String}
						 */
						"<div ng-if='fieldObject.default !== null && fieldObject.selection !== fieldObject.default'>" +
						"<div ng-repeat='field in fieldObject.conditionalFields'>" +
						"	<div generate-field check-validity='checkValidity' field-object=field></div>" +
						"</div>" +
						"</div> " +
						// "<div ng-show='{{fieldObject.id}}_form.{{fieldObject.id}}.$invalid'>invalid</div>" +
						"</span>"
					break;				
				case "textarea":
					template += 
						"<span ng-form name='{{fieldObject.id}}_form'> " +
						"<textarea ng-model='fieldObject.selection' " + 
						"name='{{fieldObject.id}}' " +
						"ng-change='checkValidity(fieldObject, {{fieldObject.id}}_form.{{fieldObject.id}}.$valid)' " +
						"ng-required='fieldObject.required'" +
						"></textarea>" +
						/**
						 * This div block is for the conditional fields driven by this one.
						 * @type {String}
						 */
						"<div ng-if='fieldObject.default !== null && fieldObject.selection !== fieldObject.default'>" +
						"<div ng-repeat='field in fieldObject.conditionalFields'>" +
						"	<div generate-field check-validity='checkValidity' field-object=field></div>" +
						"</div>" +
						"</div> " +						
						// "<div ng-show='{{fieldObject.id}}_form.{{fieldObject.id}}.$invalid'>invalid</div>" +
						"</span>"
					break;	
				case "select":
					template += 
						"<span ng-form name='{{fieldObject.id}}_form'> " +
						"<select " +
						"ng-options='item as item for item in fieldObject.value' " + 
						"ng-model='fieldObject.selection' " + 
						"name='{{fieldObject.id}}' " +
						"ng-change='checkValidity(fieldObject, {{fieldObject.id}}_form.{{fieldObject.id}}.$valid)' " +
						"ng-required='fieldObject.required'" +
						"ng-init='fieldObject.selection = fieldObject.value[0]' " +
						">" + "{{item}}" + "</select>" +
						/**
						 * This div block is for the conditional fields driven by this one.
						 * @type {String}
						 */
						"<div ng-if='fieldObject.default !== null && fieldObject.selection !== fieldObject.default'>" +
						"<div ng-repeat='field in fieldObject.conditionalFields'>" +
						"	<div generate-field check-validity='checkValidity' field-object=field></div>" +
						"</div>" +
						"</div> " +						
						// "<div ng-show='{{fieldObject.id}}_form.{{fieldObject.id}}.$invalid'>invalid</div>" +
						"</span>"
					break;
				default:
					break;
			}
			template += "</span>"

			/**
			 * We're going to compile the template string.
			 * And then we use the link function that the compile service 
			 * returns to link the element to the scope of this directive.
			 * After that, we have a live element that we can append to the current element.
			 */
			element.append($compile(angular.element(template))(scope));

			/**
			 * This interval is used to watch for the completion of putting the elements on the DOM
			 * We can set this asynchromous callback and then cancel it when we're done.
			 */
			var checkExist = setInterval(function() {
				var formObject = document.getElementsByName(scope.fieldObject.id + "_form");
			   	if (formObject.length) {
					// console.log("Exists!");
					// console.log(formObject)
					// scope.valid = scope.name_form.name.$valid;
					var identifier = scope.fieldObject.id + "_form";
					// scope.validity.values.push(scope[identifier][scope.fieldObject.id]);
					// scope.validity.values.push(scope[identifier]);
					clearInterval(checkExist);
			   	}
			}, 100)


			console.log('link function for', scope.fieldObject.id)
		}
		

	}
})